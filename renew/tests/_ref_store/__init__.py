#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from renew.tests import common_molds
from ._sub_other import other

__all__ = ['one', 'other']

one = common_molds.One(
    u'p\u0105k',
    u'k\xf3r',
    u'\u26fd',
    u' \u2026 and \u2603 and some others: \u26bd \u26c5.',
)
