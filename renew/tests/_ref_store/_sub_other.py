#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from renew.tests import common_molds

other = common_molds.Three(
    cz=u'Nechť již hříšné saxofony ďáblů rozezvučí síň úděsnými tóny waltzu, tanga a quickstepu.',
    en=u'The quick brown fox jumps over the lazy dog.',
    gr=u'ζίες καὶ μυρτιὲς δὲν θὰ βρῶ πιὰ στὸ χρυσαφὶ ξέφωτο.',
    hu=u'Jámbor célú, öv ügyű ex-qwan ki dó-s főz, puhít.',
    ku=u'Jalapeño',
    pl=u'Pchnąć w tę łódź jeża lub ośm skrzyń fig.',
    ru=u'Любя, съешь щипцы, — вздохнёт мэр, — кайф жгуч.',
)
