# -*- encoding: utf-8 -*-
import codecs
import os
import sys

import fsforge
import pytest

import renew
from renew.tests import common_molds

if sys.version[0] == '2':
    U_TEXT = unicode
else:
    U_TEXT = str


@pytest.fixture
def tmp_db_path(request, tmpdir):
    place = tmpdir.join(request.node.name)
    return place


class MyStorage(renew.PyStorage):
    integer = renew.Label(int)
    string = renew.Label(str)


@pytest.fixture
def fs_snap(tmp_db_path):
    reading_masks_ = {"**": fsforge.reading_file_processor}
    sizing_masks_ = {"**": lambda path_: os.path.getsize(path_)}

    class FsHelper(object):
        """ The class acts just as a namespace. Collects several methods under one dot-attributable object. """
        root_dir = os.path.dirname(str(tmp_db_path))

        @staticmethod
        def get_pkg_content(reading_masks=None):
            def get_(a_dict, pth):
                for n, v in a_dict.items():
                    if isinstance(v, str):
                        yield "/".join((pth, n)), v
                    elif isinstance(v, dict):
                        for sub_path, sub_text in get_(v, "/".join((pth, n))):
                            yield sub_path, sub_text

            return dict(get_(FsHelper.do_capture(reading_masks), '.'))

        @classmethod
        def do_capture(cls, reading_masks=None):
            reading_masks = reading_masks or reading_masks_
            if reading_masks == "sizes":
                reading_masks = sizing_masks_
            return fsforge.take_fs_snapshot(cls.root_dir, reading_masks)

        @staticmethod
        def read(*path_):
            path_obj = tmp_db_path
            for p in path_:
                path_obj = path_obj.join(p)
            if not path_obj.exists():
                pytest.fail("Cannot read the file. Doesn't exist: %s (full path: %s)" % ("/".join(path_), path_obj))
            with codecs.open(str(path_obj), "rb", encoding="utf-8") as f:
                return f.read()

    return FsHelper


@pytest.fixture
def simple_scene(tmp_db_path):
    return MyStorage(str(tmp_db_path))


def test_creating_empty(simple_scene, fs_snap):
    assert simple_scene.integer == 0
    assert simple_scene.string == ""
    assert fs_snap.get_pkg_content() == {}

    with simple_scene.updating_db():
        pass

    expected_init = """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/


__all__ = ['integer', 'string']

integer = 0

string = ''
"""
    fs_snapshot = fs_snap.get_pkg_content()

    assert len(fs_snapshot) == 1
    assert './test_creating_empty/__init__.py' in fs_snapshot
    assert fs_snapshot['./test_creating_empty/__init__.py'] == expected_init


def test_setattr_restrictions(simple_scene):
    msg = r"Unexpected assignment of type str to MyStorage class, attribute not_existing."
    with pytest.raises(AttributeError, match=msg):
        simple_scene.not_existing = "some"


@pytest.mark.parametrize("way", ["one", "another"])
def test_no_leak_across_scenes(tmp_db_path, fs_snap, way):
    db = MyStorage(str(tmp_db_path))
    db2 = MyStorage(str(tmp_db_path.join("other_scene")))

    assert db.integer == 0
    assert db.string == ""

    if way == "one":
        with db.updating_db():
            db.integer = 455
            db.string = "some string"
    else:
        with db.updating_db() as store:
            store.integer = 455
            store.string = "some string"

    assert db.integer == 455
    assert db.string == "some string"

    assert db2.integer == 0
    assert db2.string == ""


def test_bad_default_value(simple_scene):
    msg = (
        r"The object passed to 'Label' class \(type: '12345'\) has to be a callable wih no args and return anything. "
        r"Failed to call or get return value from 12345. Got TypeError: 'int' object is not callable"
    )
    with pytest.raises(TypeError, match=msg):
        class _(renew.PyStorage):
            value = renew.Label(12345)


def test_not_existing_attribute(simple_scene):
    msg = "'MyStorage' object has no attribute 'not_exising'"
    with pytest.raises(AttributeError, match=msg):
        _ = simple_scene.not_exising


def test_bad_assignment(simple_scene):
    msg = "Cannot assign a different type than 'int', got 'float'."
    with pytest.raises(AttributeError, match=msg):
        simple_scene.integer = 3.14159


def test_split_path_and_name():
    n = os.path.normpath

    def d(posix_path):
        """ normalize and make testable absolute path """
        this_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        return os.path.join(this_dir, *n(posix_path).split(os.sep))

    s = renew._serializer._split_path_and_name
    assert s(d("one/two/tree.py")) == (d("one/two"), "tree")
    assert s(d("one/two/__init__.pyc")) == (d("one"), "two")
    assert s(d("one/two/tree.py")) == (d("one/two"), "tree")
    assert s(d("one/two/tree.python")) == (d("one/two"), "tree.python")
    assert s(d("one/two/tree.python.py")) == (d("one/two"), "tree.python")
    assert s(d("one/two/__init__.py")) == (d("one"), "two")
    assert s(d("one/two/__init__.py")) == (d("one"), "two")
    assert s(__file__) == (d("tests"), 'test_dber')


class StorageWithRefs(renew.PyStorage):
    reffed_string = renew.Reference(U_TEXT)
    reffed_bool = renew.Reference(bool)
    labeled_list = renew.Label(list)
    info = renew.Label(str)


def test_split_references(tmp_db_path):
    scene_with_refs = StorageWithRefs(str(tmp_db_path))
    a, b = scene_with_refs._split_references()
    assert set(a) == {'info', 'labeled_list'}
    assert set(b.values()) == {'reffed_string', 'reffed_bool'}


def test_split_references_new_attrs(simple_scene):
    a, b = simple_scene._split_references()
    assert set(a) == {'string', 'integer'}
    assert set(b.values()) == set()

    # happy when no raise on that:
    simple_scene.train = renew.LiveReference(common_molds.Three())
    simple_scene.signs = renew.LiveReference(False)
    simple_scene.roads_list = renew.LiveReference(common_molds.One())

    a, b = simple_scene._split_references()
    assert set(a) == {'string', 'integer'}
    assert set(b.values()) == {'roads_list', 'train', 'signs'}


def test_bad_reference_assignment(simple_scene):
    msg = r"Unexpected assignment of type \w+ to MyStorage class, attribute not_existing. " \
          "In order to set live objects encapsulate its values with renew.LiveLabel or renew.LiveReference."
    with pytest.raises(AttributeError, match=msg):
        simple_scene.not_existing = renew.Label(str)

    with pytest.raises(AttributeError, match=msg):
        simple_scene.not_existing = renew.Reference(str)


def test_storing(fs_snap, tmp_db_path, is_py2):
    scn = StorageWithRefs(str(tmp_db_path))
    with scn.updating_db():
        scn.reffed_string = u"This string is referenced and contains a sentence. It is just a test purpose."
        scn.info = "Informative string."

    with scn.updating_db():
        scn.labeled_list = [1, 2, 3, 4]

    assert fs_snap.read("__init__.py") == u"""\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from ._sub_reffed_bool import reffed_bool
from ._sub_reffed_string import reffed_string

__all__ = ['info', 'labeled_list', 'reffed_bool', 'reffed_string']

info = 'Informative string.'

labeled_list = [1, 2, 3, 4]
"""

    if is_py2:
        assert fs_snap.read("_sub_reffed_string.py") == u"""\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/


reffed_string = u'This string is referenced and contains a sentence. It is just a test purpose.'
"""
    else:
        assert fs_snap.read("_sub_reffed_string.py") == u"""\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/


reffed_string = 'This string is referenced and contains a sentence. It is just a test purpose.'
"""

    assert fs_snap.read("_sub_reffed_bool.py") == u"""\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/


reffed_bool = False
"""

    recreated = StorageWithRefs(str(tmp_db_path))

    assert recreated.info == scn.info
    assert recreated.reffed_string == scn.reffed_string
    assert recreated.reffed_bool == scn.reffed_bool
    assert recreated.labeled_list == scn.labeled_list == [1, 2, 3, 4]


@pytest.mark.parametrize("way", ["one", "another"])
def test_not_existing_db(tmp_db_path, way):
    if way == "one":
        db_file = str(tmp_db_path)
    else:
        db_file = str(tmp_db_path.join("__init__.py"))
        assert not os.path.exists(db_file)

    scn = StorageWithRefs(db_file)
    assert scn.info == ""
    assert scn.reffed_string == ""
    assert scn.reffed_bool is False
    assert scn.labeled_list == []


def test_references_pods(tmp_db_path, fs_snap):
    class TheStore(renew.PyStorage):
        one = renew.Reference(list)
        two = renew.Label(list)

    primary = TheStore(str(tmp_db_path))

    one = [1, 2, 3, 4]
    with primary.updating_db():
        primary.one = one
        primary.two = [0, one, 2, ({"a": 1.0, "b": 4.2, "c": True, "once again": one})]

    snap = fs_snap.do_capture("sizes")
    assert list(snap) == ['test_references_pods']
    assert set(snap['test_references_pods']) == {'__init__.py', '_sub_one.py'}

    assert fs_snap.read("__init__.py") == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from ._sub_one import one

__all__ = ['one', 'two']

two = [0, one, 2, {'a': 1.0, 'b': 4.2, 'c': True, 'once again': one}]
"""
    assert fs_snap.read("_sub_one.py") == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/


one = [1, 2, 3, 4]
"""

    recreated = TheStore(str(tmp_db_path))
    assert recreated.one == primary.one
    assert recreated.two == primary.two


def test_cross_references_with_molds(tmp_db_path, fs_snap):
    class TheStore(renew.PyStorage):
        one = renew.Reference(common_molds.One)
        two = renew.Label(common_molds.Two)
        three = renew.Reference(common_molds.Three)
        cross = renew.Label(common_molds.One)

    db = TheStore(str(tmp_db_path))

    one_ = common_molds.One(1, 2, 3, 4)
    three_ = common_molds.Three(aa=one_, bb=[one_, "m15"], cc=None, dd=3.14159, ee=False, ff="null")
    two_ = common_molds.Two(one_, "others", three_)

    extra_reference_ = [(1, one_), (3, three_)]

    with db.updating_db():
        db.one = one_
        db.two = two_
        db.three = three_
        db.cross = common_molds.One(one_, '1', two_, '2', three_, '3')

    with db.updating_db():
        db.extra_reference = renew.LiveReference(extra_reference_)

    snap = fs_snap.do_capture("sizes")
    assert list(snap) == ['test_cross_references_with_molds']
    assert set(snap['test_cross_references_with_molds']) == {
        '__init__.py', '_sub_one.py', '_sub_three.py', '_sub_extra_reference.py',
    }

    assert fs_snap.read("__init__.py") == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from renew.tests import common_molds
from ._sub_extra_reference import extra_reference
from ._sub_one import one
from ._sub_three import three

__all__ = ['cross', 'extra_reference', 'one', 'three', 'two']

cross = common_molds.One(one, '1', common_molds.Two(one, 'others', three), '2', three, '3')

two = common_molds.Two(one, 'others', three)
"""

    assert fs_snap.read("_sub_one.py") == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from renew.tests import common_molds

one = common_molds.One(1, 2, 3, 4)
"""

    assert fs_snap.read("_sub_three.py") == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from renew.tests import common_molds
from ._sub_one import one

three = common_molds.Three(aa=one, bb=[one, 'm15'], cc=None, dd=3.14159, ee=False, ff='null')
"""
    assert fs_snap.read("_sub_extra_reference.py") == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from ._sub_one import one
from ._sub_three import three

extra_reference = [(1, one), (3, three)]
"""

    recreated_store = TheStore(str(tmp_db_path))
    assert recreated_store.one == one_
    assert recreated_store.two == two_
    assert recreated_store.three == three_
    assert recreated_store.extra_reference == extra_reference_


def test_that_common_molds_are_sane():
    items = (
        ('element_1', common_molds.One(12, 35)),
        ('element_2', common_molds.Two(34, 1)),
        ('three', common_molds.Three()),
    )
    code = renew._py_lang.build_py_file_content(items)

    expected_code = """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from renew.tests import common_molds

element_1 = common_molds.One(12, 35)

element_2 = common_molds.Two(34, 1)

three = common_molds.Three()
"""
    assert code == expected_code
    # test pass if that call below doesn't raise
    exec(code.encode("utf-8"))


class SceneWithDict(renew.PyStorage):
    tre = renew.Label(list)
    collection = renew.Label(dict)
    fixed_thing = renew.Reference(str)

    def own_update_method(self, **new_entries):
        for name, value in new_entries.items():
            if "u" in name and "o" in name:
                setattr(self, name, renew.LiveReference(value))
            else:
                setattr(self, name, value)
            self.collection[name] = value


@pytest.fixture
def dicted_scene(tmp_db_path):
    return SceneWithDict(str(tmp_db_path))


def cls_attr_type(obj, name):
    return type(object.__getattribute__(obj, name))


def test_refs_in_dict(tmp_db_path, dicted_scene, fs_snap):
    with dicted_scene.updating_db():
        dicted_scene.fixed_thing = "That's a fixed label."
        dicted_scene.own_update_method(uno=[1], duo=[2, 2], tre=[3, 3, 3], mould=common_molds.Three(m1=11, m2=22))
        dicted_scene.ext_label = renew.LiveLabel(common_molds.One("ext", "label"))
        dicted_scene.ext_ref = renew.LiveReference(common_molds.Two("ext", "reference"))

    # the assigned values should be readable
    assert dicted_scene.uno == [1]
    assert dicted_scene.duo == [2, 2]
    assert dicted_scene.tre == [3, 3, 3]
    assert dicted_scene.mould == common_molds.Three(m1=11, m2=22)
    assert dicted_scene.ext_label == common_molds.One("ext", "label")
    assert dicted_scene.ext_ref == common_molds.Two("ext", "reference")
    assert dicted_scene.fixed_thing == "That's a fixed label."
    assert dicted_scene.collection == {
        'duo': [2, 2],
        'mould': common_molds.Three(m1=11, m2=22),
        'tre': [3, 3, 3],
        'uno': [1],
    }

    expected_types = {
        "uno": renew.LiveReference,
        "duo": renew.LiveReference,
        "tre": renew.Label,
        "mould": renew.LiveReference,
        "ext_label": renew.LiveLabel,
        "ext_ref": renew.LiveReference,
        "fixed_thing": renew.Reference,
        "collection": renew.Label,
    }
    for name, type_ in expected_types.items():
        assert cls_attr_type(dicted_scene, name) == type_

    snap = fs_snap.get_pkg_content()
    assert len(snap) == 6
    assert set(snap) == {
        './test_refs_in_dict/__init__.py',
        './test_refs_in_dict/_sub_duo.py',
        './test_refs_in_dict/_sub_ext_ref.py',
        './test_refs_in_dict/_sub_fixed_thing.py',
        './test_refs_in_dict/_sub_mould.py',
        './test_refs_in_dict/_sub_uno.py',
    }

    assert snap['./test_refs_in_dict/__init__.py'] == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from renew.tests import common_molds
from ._sub_duo import duo
from ._sub_ext_ref import ext_ref
from ._sub_fixed_thing import fixed_thing
from ._sub_mould import mould
from ._sub_uno import uno

__all__ = ['collection', 'duo', 'ext_label', 'ext_ref', 'fixed_thing', 'mould', 'tre', 'uno']

collection = {'duo': duo, 'mould': mould, 'tre': [3, 3, 3], 'uno': uno}

ext_label = common_molds.One('ext', 'label')

tre = [3, 3, 3]
"""

    assert snap['./test_refs_in_dict/_sub_fixed_thing.py'] == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/


fixed_thing = "That's a fixed label."
"""

    assert snap['./test_refs_in_dict/_sub_uno.py'] == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/


uno = [1]
"""

    assert snap['./test_refs_in_dict/_sub_duo.py'] == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/


duo = [2, 2]
"""
    assert snap['./test_refs_in_dict/_sub_ext_ref.py'] == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from renew.tests import common_molds

ext_ref = common_molds.Two('ext', 'reference')
"""

    recreated = SceneWithDict(str(tmp_db_path))
    assert recreated.uno == [1]
    assert recreated.duo == [2, 2]
    assert recreated.tre == [3, 3, 3]
    assert recreated.mould == common_molds.Three(m1=11, m2=22)
    assert recreated.ext_label == common_molds.One("ext", "label")
    assert recreated.ext_ref == common_molds.Two("ext", "reference")
    assert recreated.collection == {
        'duo': [2, 2],
        'mould': common_molds.Three(m1=11, m2=22),
        'tre': [3, 3, 3],
        'uno': [1],
    }

    for name, type_ in expected_types.items():
        assert cls_attr_type(recreated, name) == type_, "name %s is not type %s" % (name, type_.__name__)

    recreated.mould = common_molds.Three(nothing=None)
    assert recreated.mould == common_molds.Three(nothing=None)
    assert cls_attr_type(recreated, "mould") == renew.LiveReference

    with recreated.updating_db():
        recreated.own_update_method(mould=common_molds.Three(m1=345, m2=678, m3=333))

    assert recreated.mould == common_molds.Three(m1=345, m2=678, m3=333)
