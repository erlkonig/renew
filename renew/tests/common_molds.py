import renew


class One(renew.Mold):
    _cls_dependency = "renew.tests"
    _cls_namespace = "common_molds"

    def __init__(self, *elements):
        self.elements = elements


class Two(renew.Mold):
    _cls_dependency = "renew.tests"
    _cls_namespace = "common_molds"

    def __init__(self, *elements):
        self.elements = elements


class Three(renew.Mold):
    _cls_dependency = "renew.tests"
    _cls_namespace = "common_molds"

    def __init__(self, **things):
        self.things = things
